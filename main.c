/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdbool.h"
#include "stdio.h"
/*#include "usbuartio.h"*/

int main(void)
{
    CyGlobalIntEnable; // Enable global interrupts. 
    
    LCD_Start(); // Inicializacion del LCD
    
    
    bool estado = 0; //booleano para encender o apagar los LEDs 
    
    for(int ciclos = 0;ciclos < 1000;ciclos++) 
    {
        LCD_ClearDisplay();

        //Proceso de escritura en LCD
        LCD_Position(0,0);
        LCD_PrintString("LEDS");
        LCD_Position(0,6);
        
        LCD_PrintString("Ciclos:");
        if (ciclos<10){
        LCD_Position(0,15);
        }
        if ((ciclos>=10) && (ciclos<100)){
            
        LCD_Position(0,14);
        }
        LCD_PrintNumber(ciclos);
        
        
        LCD_Position(1,0);
        LCD_PrintString("Izq:");
        LCD_Position(1,8);
        LCD_PrintString("Der:");
        
        //Proceso de encendido y apagado de los LED
        LED3_Write(~estado);
        LCD_Position(1,4);
        LCD_PrintString("ON");
        CyDelay(200);
       
        LED3_Write(estado);
        LCD_Position(1,4);
        LCD_PrintString("  ");
        CyDelay(200);
        
        LED2_Write(~estado);
        LCD_Position(1,12);
        LCD_PrintString("ON");
        CyDelay(300);
       
        LED2_Write(estado);
        LCD_Position(1,12);
        LCD_PrintString("  ");
        CyDelay(300);
        
        if(ciclos == 999){
            ciclos = -1;
        }
        
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
