%   Proyecto corto - Gr�a 
%   Jos� Mar�a Jim�nez Coronado 2016112170
clc; clear;
%%  Importaci�n de datos
A = importdata("GRUA_TRAPEX.CSV");      % Importa datos
Tiempo      = A.data(:,1);              % Guarda vector de tiempo
Entrada     = A.data(:,2);              % Guarda vector de entrada
Angulo      = A.data(:,3);              % Guarda vector de �ngulo
Posicion    = A.data(:,4);              % Guarda vector de posici�n

%%  System Identification Toolbox
%systemIdentification('sysIdent.sid')    % Abre el System Identification Tool

%%  Importaci�n de modelos obtenidos en la System Identification Toolbox
load('transferFunctions.mat');          % TFs de �ngulo y posici�n

%%  Mostrar funciones de transferencia obtenidas
disp("*---Funci�n de transferencia hacia posici�n")
zpk(tfPosicion)                         % Muestra el modelo de la posici�n en forma zpk
disp("*---Funci�n de transferencia hacia �ngulo")
zpk(tfAngulo)                           % Muestra el modelo del �ngulo en forma zpk

%% Creaci�n del modelo en espacio de estados
A       = [0 0 1 0;                     % Generaci�n de la matriz A del modelo
           0 0 0 1;
           0 0 -17.72 0;
           0 -35.32 0 -0.03409];
B       = [0;-0.3996;0.42393;-0.5454];       % Generaci�n del vector B del modelo
C       = [1 0 0 0];                    % Generaci�n de la matriz C del modelo
sysGrua = ss(A, B, C, 0);               % Inicializaci�n del modelo en espacio de estados
disp("*---Modelo en espacio de estados del sistema")
ss(A, B, C, 0)                          % Muestra el modelo en espacio de estados

%% C�lculo del controlador REI usando LQR
As      = [A [0;0;0;0];-C 0];           % Matriz aumentada A            
Bs      = [B;0];                        % Matriz aumentada B
Q       = eye(5)*100;                   % Matriz identidad 5x5
Q(1,1)  = 6000;                         % Modificaci�n Q(posici�n)
Q(2,2)  = 5;                          % Modificaci�n Q(�ngulo)
Q(3,3)  = 1;                          % Modificaci�n Q(velocidad lineal)
Q(4,4)  = 5;
R       = 1;                            % Constante R para lqr
Ks      = lqr(As, Bs, Q, R);            % Se calculan las constantes mediante lqr
disp("*---Vectores K y Ki--- LQR")
K       = Ks(1:4)                      % Se extraen las constantes para K
Ki      = -Ks(5)*4.15                   % Se extrae el negativo de Ki
%eig(As-Bs*Ks)

%% Calculo del controlador REI usando ubicacion de polos
%eig (As-Bs*Ks);                          %extrae polos actuales
P= [-1.1+0.5i -1.1-0.5i -5.6 -5.7 -5.8];  % posiciona nuevos polos
Ks= place (As,Bs,P)    ;                  % Se calculan las constantes
disp("*---Vectores K y Ki---ubicacion polos")
K=Ks(1:4)                               %% Se extraen las constantes para K
Ki= -Ks(5)                              %% Se extraen las constantes para Ki